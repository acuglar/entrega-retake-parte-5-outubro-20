""" # ERRO PARA COLUNAS INVÁLIDAS
class InvalidColumnFilter(ValueError):
    raise ValueError

# ERRO PRA QUANDO NENHUM DADO FOR ENCONTRADO
class NotFound(ValueError):
    raise ValueError

# ERRO, CASO O USUÁRIO NÃO PASSAR NENHUM, OU 1, ARGS NA ROTA
class MissingArgs(AttributeError):
    raise AttributeError """