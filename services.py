import csv
""" from custom_errors import InvalidColumnFilter, NotFound, MissingArgs """


class CsvServices:
    VALID_FILTERS = ("Name", "Platform",)

    def validate_fields(self, *args):
        if len(args) < 2:
            raise AttributeError({'Error': 'MissingArgs'})


    def validate_column_filter(self, column):
        valid_filters = [i.lower() for i in self.VALID_FILTERS]

        if column not in valid_filters: 
            raise ValueError({'Error': 'InvalidColumnFilter'})


    def found_by_filter(self, *args):
        
        name, column = args

        with open('games.csv') as f:
            reader = csv.DictReader(f)

            filter = [i for i in reader if name in i[column.title()].lower()]

        
        return filter


    @staticmethod
    def top_teen(data):
        return data[:10]