from flask import Flask, jsonify, request
from http import HTTPStatus
from services import CsvServices

app = Flask(__name__)

@app.route("/games", methods=['GET'])
def get_games():
    name = request.args.get("name")
    column = request.args.get("column")
    
    filters = CsvServices()

    result = filters.found_by_filter(name, column)

    return jsonify(result)